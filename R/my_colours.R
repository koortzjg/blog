my_colours <- read.table("data/blog_colours.txt", header = TRUE, sep = "|", encoding = "UTF-8")
my_colours$hex <- with(my_colours, rgb(r, g, b, maxColorValue = 255))

par(mar = c(0, 0, 0, 0))
plot(
  0,
  0,
  type = "n",
  xlim = c(0, 1),
  ylim = c(0, 1),
  axes = FALSE,
  xlab = "",
  ylab = ""
)
line = 3
col = 12
rect(
  rep((0:(col - 1) / col), line) ,
  sort(rep((0:(
    line - 1
  ) / line), col), decreasing = T) ,
  rep((1:col / col), line) ,
  sort(rep((1:line / line), col), decreasing = T),
  border = "light gray" ,
  col = my_colours$hex
)
text(rep((0:(col - 1) / col), line) + 0.02 ,
     sort(rep((0:(
       line - 1
     ) / line), col), decreasing = T) + 0.01 ,
     seq(1, 36)  ,
     cex = 0.5)

write.table(
  my_colours[1:12, 4],
  "data/hex_colours.txt",
  sep = "|",
  fileEncoding = "UTF-8",
  row.names = FALSE,
  quote = FALSE,
  col.names = FALSE
)
write.table(
  my_colours[13:24, 4],
  "data/hex_colours.txt",
  sep = "|",
  fileEncoding = "UTF-8",
  row.names = FALSE,
  quote = FALSE,
  col.names = FALSE
)
write.table(
  my_colours[25:36, 4],
  "data/hex_colours.txt",
  sep = "|",
  fileEncoding = "UTF-8",
  row.names = FALSE,
  quote = FALSE,
  col.names = FALSE
)
write.table(
  my_colours,
  "data/hex_colours.txt",
  sep = "|",
  fileEncoding = "UTF-8",
  row.names = FALSE,
  quote = FALSE
)
