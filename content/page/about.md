---
comments: false
subtitle: Things you need to know first
title: About me
---

My name is Hardi Koortzen. I have recent started as a Data Scientist at [Oxford University Press](https://global.oup.com/?cc=gb), working on understanding and visualising of various data sources. This has been challenging, as well as enjoyable. Previously, I was working in the Petrochemical field. I received my Ph.D at the [University of the Free State](https://www.ufs.ac.za/) in Chemistry. Over the past 17 years, I have worked on numerous projects, mostly involving catalyst synthesis, characterisation and evaluation. I have always enjoyed the data analysis and have set up numerous databases, with my first one in `MS Access` and the most recent in `SQL` (which I now prefer). I have used [R](https://www.r-project.org/) and [RStudio](https://www.rstudio.com/) since 2014, and now have moved completely away from `MS Excel` for my data wrangling and analysis.

As a scientist I have a few publications and patents, which I am really proud of. There should have definitely been more, but I can always use the excuse of working on proprietary projects.

I consider myself a *'traveller'* as I have a rich history; born in Namibia, grew up in South Africa, now living in England. I have spend some time in Scotland, and would not mind going back there. One day I might return there. I watch rugby, when on BBC or ITV, and am an avid supporter of South Africa, otherwise, it is the home nations.
