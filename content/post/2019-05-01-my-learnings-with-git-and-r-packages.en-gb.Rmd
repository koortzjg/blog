---
title: My Learnings with Git and R Packages
author: Hardi Koortzen
date: '2019-05-01'
slug: my-learnings-with-git-and-r-packages.en-gb
categories:
  - git
tags: 
  - git
  - R
type: ''
subtitle: ''
image: ''
---

When I set out to create my own website, I was/am still very nervous. This is so outside my comfort zone, that I cannot even describe it to someone. Fortunately, there are a few really talented people in the world, and [Yihui Xie](https://yihui.name/) is one of them. When I first started my `R` journey (somewhere in 2014), the packages `knitr` and `rmarkdown` was already in use. This allowed me to organise my thoughts, and produce several documents outputs, to be shared by others in my team. This was also helped with the IDE from [`RStudio`](https://www.rstudio.com/), which was my first, and to date, only IDE for `R`. This combination really helped me, and resulted in the adoption, at least for me, by using `R` rather than `Excel` for all my data analysis work. I have learned so much, that I am not even sure where to begin, and the work by [Hadley Wickham](http://hadley.nz/), further helps here. Now there are certainly more people to give credit to on my `R` journey, however, these really made an impression and assisted me in 'getting out there'.

Now, this journey was not painless. As a non-developer, I have not worked with [`git`](https://git-scm.com/), or [`github`](https://en.wikipedia.org/wiki/GitHub) before. My first experience with this was with `RStudio`. I tried to use it, however, I always fell short and the struggle to keep it up was not worth the effort. That is until [Jenny Bryan](https://github.com/jennybc) excellent resource on this [topic](http://happygitwithr.com/). This kept me going, and I started to use it more and more. Since my company does not pay for this subscription, I found [gitlab](https://gitlab.com/), more useful, and started to use it for both private and public data. This privacy, gave me the required motivation to use git.

Then I started with a [website](https://hardikoortzen.netlify.com/), and a whole world of git opened up. Now there were things called `branches`, `.gitignore`, and loads more. I suddenly felt so inadequate, and thought **'Not this again!!!!'**. As most of you know by now, the `blogdown` package helps here tremendously. However, that is just the start of the rabbit hole. Suddenly there are files called `config.toml`, and all this requires `git`. This is just too much. Closed `RStudio` and work on some other reports. Weeks/months later, I pick this up again, however, this time I follow the advise from Jenny and Yihui to the letter. Running into a few errors, and googling it, and finally, things are starting to progress. Things were really plodding along, and I managed a few posts, without any `branches`. Then I realised that I might have added some private files to the git repo (look at me, using big words). Although I have transformed the data into something not recognisable as raw data, I still wanted to remove it.

Removing this was not easy, and required some googling, again. Some more copy and pasting of code that gave error upon error for me. Not this again!!! Since I do not have people using this, I cannot just ask for help with the code I am pasting. Ah, eventually figured out what that pesky `\` means in the code. Now I can delete my file [history](http://bit.ly/git_remove_sensitive_data). What have I learned here? Well to start off, copying and pasting is very difficult on the command line. Copying the following code, results in error:

```
git filter-branch --force --index-filter \
'git rm --cached --ignore-unmatch PATH-TO-YOUR-FILE-WITH-SENSITIVE-DATA' \
--prune-empty --tag-name-filter cat -- --all
```

Why? I eventually realised that the `\` character is just for making the code on the website more readable, and all of this needs to be a one liner. This should have been obvious, however, saying it again here, not a developer. Now, they also mentioned something on the file called `.gitignore` and how to add these files there. Ah, yes, make sense, so let me just do as they said. This does not work! How? Why? Again the doubt. It is getting late, and I need sleep, will pick this up later. Googling, again, seems I am getting better at googling than anything else. At least I find [this](http://bit.ly/git_ignore_r). Now looking at this, does not immediately make it clear. OK, so after my manual addition to `.gitignore`, the file was wrapped in quotes `"YOUR-FILE-WITH-SENSITIVE-DATA"`, and in the `.gitignore` file, it should not be, i.e. `YOUR-FILE-WITH-SENSITIVE-DATA`. I also learned that you can remove all files with a certain pattern (`*-ex.R`) (notice, no quotes), and immediately implement that in my code. Success. I am unstoppable.

Now I have created this `branch` on `git`, and it is time to add this to the `master`. In gitlab, there are things called `merge requests`, so I start there. A few clicks later, and the `branch` are all committed (again, big words). Looking at my local repo, I can still see the branch, even on the `REMOTE` one. Why, oh why? My friend, Google, here again, and the answer [is](http://bit.ly/git_branch_delete). This is easy right. Yes, I manage to delete the `LOCAL` one, however, the `REMOTE` one is still there and when I try to delete, I get errors. OK, next google answer, and it seems that I need to `git fetch -p`, see [here](http://bit.ly/git_branch_remote), although I have `Pull` and `Push` from `RStudio`. OK, success. Now it seems that I am not good at organising all these learnings, and decided to blog about it. That way, I have all links, and the code here.

```
git push branch --delete <branch_name>

git fetch -p
```

Well, that was quite a story, although not in the correct order, but that does not matter, as long as it is capture somewhere. I also realise that I use some different conventions in my code, and prefer some other `magrittr` glyphs as well. It is difficult enough in `R` typing everything, and having to type these as well, is too repetitive. Even `RStudio` have a short cut for the forward pipe operator (`%>%`, `Ctrl + Shift + M`), why not have that for the rest as well. Ah, yes, let's create a package, and also include some `ggplot2` colours and some general functions in there. Creating a package is easy right. Right?

The short answer here is 'Yes', however, for me it was a bit of a journey. At least there were others before me, see [this](http://r-pkgs.had.co.nz/) for a excellent overview. After creating a few functions, and loading the package, I was unable to use these new functions. I have done all as I was told, and I can see the documentation for the functions, but cannot use them. It turns out that I needed to export these, `@export` in order to use it. Here is a function I created for this package, and is working now:

```{r, eval = FALSE}
#' Scale the data using the feature scaling or just the \code{scale()} function
#'
#' @param x numeric vector required for function
#' @param a number of lowest arbitrary point. Default is 0
#' @param b number of highest arbitrary point. Default is 1
#' @param .scale a Boolean, indicating whether to use the \code{scale()} function
#' @param ... additional arguments for the \code{scale()} function
#' @return A numeric vector where the minimum is equal to \code{a}, and the
#'         maximum equal to \code{b}. If \code{.scale = TRUE}, then will return
#'         the vector output of the \code{scale()} function.
#' @examples
#' # scale the cyl to be between 0 and 1
#' feature_scaling(mtcars$cyl)
#'
#' # scale the cyl to be between -1 and 1
#' feature_scaling(mtcars$cyl, -1, 1)
#'
#' # using the scale() function
#' feature_scaling(mtcars$cyl, .scale = TRUE)
#' @export
feature_scaling <- function(x,
                            a = 0,
                            b = 1,
                            .scale = FALSE,
                            ...) {
  x_clean <- na.omit(x)
  
  if (.scale) {
    as.vector(scale(x_clean, ...))
  } else {
    a + ((x_clean - min(x_clean)) * (b - a)) / (max(x_clean) - min(x_clean))
  }
}

```

Notice the `@export` just before defining the function. That allows the function to be used after loading the package. Now, I still need some error checking and warnings, however, for now this works, and since this is not on CRAN or available on github, I at least still remember how to use it without errors. Maybe in the next version of this, there will be more checking and warnings.

This was also a good opportunity to add the `magrittr` pipe operators to the package, that way, I can only call this package, and then still be able to use these. This is not to recreate these, but rather just export them from the package.

```{r, eval = FALSE}
#' Pipe operator
#'
#' See \code{magrittr::\link[magrittr]{\%>\%}} for details.
#'
#' @name %>%
#' @rdname pipe
#' @keywords internal
#' @export
#' @importFrom magrittr %>%
#' @usage lhs \%>\% rhs
NULL
```

Unfortunately, I do not have the link to this, apologies, however, this works fine. By replacing the pipe glyph (`%>%`), with the others from `magrittr`, all can be exported, i.e. `%<>%`, `%T>%`, `%$%`. Now, when your package is loaded, these are available, and there is no need to call the `magrittr` package.

Now, as I have mentioned before, `RStudio` have a short cut for the forward pipe operator (`%>%`, `Ctrl + Shift + M`), and it would be really useful to have short cuts for the others as well. This is where I got stumped, again. Fortunately, help was at [hand](https://rstudio.github.io/rstudioaddins/). I could add custom short cuts to the Addins, but first I needed to create the required addins. OK, this should not be that difficult. Famous last words. Although I have created the addins, it still did not show up under the Addins.

```{r, eval = FALSE}
#' Insert \%T>\% .
#'
#' Call this function as an addin to insert \code{\link[magrittr]{\%T>\%}}
#' (Magrittr pipe, \url{https://CRAN.R-project.org/package=magrittr}) at the
#' cursor position.
#'
#' @export
insertMagrittrTeePipeAddin <- function() {
  rstudioapi::insertText(" %T>%")
}
```

Well, back to the website again. You also need to 'register' the custom addins, by creating a file (`addins.dcf`), in the `inst/rstudio/` directory of your package. How could I have missed that? Registering the addins are easy:

```{r, eval = FALSE}
Name: Insert %T>%
Description: Inserts %T>% (Magrittr tee pipe) at the cursor position.
Binding: insertMagrittrTeePipeAddin
Interactive: false
```

The way this works, my understanding, is that you do not even have to have your package loaded, your addins will show up, and this is due to you registering it in the `addins.dcf` file.

Wow, I was so pleased after that. I have learned so much, and have a working package containing some useful functions, my blog colours, and addins. The power we have in `R` and `RStudio`, is awesome, and it is thanks to so many talented individuals. Now all I need to do is to write about how to use `R`, and how useful it is in my daily life. With these short cuts, typing is easy and makes life a bit easier.

Creating this blog has taught me quite a bit about `git`, `R`, and `RStudio` and how useful they are. There is no need to keep pasting code from previous posts, just create an addin, or even a `snippet` and you are set. Making changes to your posts is easy, and `git` keeps tracks of the progress, and if there are issues, they can be rectify (still need to experience this). The website is updated within minutes after the commit.
