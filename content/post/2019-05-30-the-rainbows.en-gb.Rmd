---
title: The Rainbows
author: Hardi Koortzen
date: '2019-05-30'
slug: the-rainbows.en-gb
categories:
  - General
tags:
  - Dogs
  - R
type: ''
subtitle: ''
image: ''
---

We are getting a new dog. This was a long time coming, and it is finally time to get another dog. We already have a [Miniature Pinscher](https://en.wikipedia.org/wiki/Miniature_Pinscher) named *Coco*. She is a good dog, although, true to her breed, and as such, our post comes with bite marks. She can be a bit annoying, as they tend to bark quite a bit. She has been lonely, and now it is time to get her someone to growl at, and maybe share a sofa. Our puppies will be a [Rhodesian Ridgeback](https://en.wikipedia.org/wiki/Rhodesian_Ridgeback). Although the pup is not yet with us, he is already named, *Zuko*. He has a lovely breeder, and we are part of their Facebook page. We were late to the party, since some of the new owners was already waiting for their pups. We were also very lucky, since there were more puppies than what was originally thought, so we could have one. It was an interesting experience so far. We were screened, to make sure we will look after him, with love and care, and also needed the 'approval' of their male Rhodesian Ridgeback, which was so big and so lovely, that I just wanted to take him home. In the end, this was all worth it, since we will be getting *Ernie* from the *Rainbows* on Friday the 31st of May 2019. The household is generally so excited. We will not be getting too much sleep Friday evening, since we need to also introduce him to *Coco*, and make sure he his settled in properly.

Although this seems fun, I would rather look at some interesting things on how the puppies developed. The breeders are such lovely people, and they went through so much trouble to keep us updated, daily pictures, etc. One thing that was truly interesting was the growth of the puppies. These are big dogs, and as such needs to develop into their size. Let's look at the weights of the puppies.

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  echo      = FALSE,
  error     = TRUE,
  message   = FALSE,
  warning   = FALSE,
  fig.align = "center",
  fig.asp   = 0.618,
  fig.width = 8,
  out.width = "95%"
)
if (!("rforblog" %in% (.packages()))) {
  suppressPackageStartupMessages(library("rforblog"))
  extrafont::loadfonts(device = "win", quiet = TRUE)
}
if (!("tidyverse" %in% (.packages()))) {
  suppressPackageStartupMessages(library("tidyverse"))
  theme_set(blog_theme())
}
```

```{r mass-data}
mass_data <- read_csv("data_raw/mass.csv") %>%
  gather(key = "Rainbows", value = "Mass", -Date) %>%
  inner_join(read_csv("data_raw/rainbows.csv"), by = "Rainbows") %>%
  mutate(Mass = Mass / 1000)

zuko_data <- read_csv("data_raw/zuko.csv") %>%
  gather(key = "Rainbows", value = "Mass", -Date) %>%
  inner_join(read_csv("data_raw/rainbows.csv"), by = "Rainbows") %>%
  mutate(Mass = Mass / 1000)
```

```{r fig-mass-points, fig.cap = "The weights of the puppies, by sex. The highlighted points is for Zuko."}
ggplot(mass_data,
       aes(
         x = Date,
         y = Mass,
         colour = Rainbows,
         shape = Sex
       )) +
  geom_point(alpha = 0.2,
             show.legend = FALSE) +
  geom_point(data = zuko_data,
             show.legend = FALSE) +
  geom_smooth(data = zuko_data,
              se = FALSE,
              show.legend = FALSE,
              size = 0.5) +
  blog_scale_colour() +
  facet_wrap( ~ Sex) +
  scale_x_date(date_breaks = "1 month",
               date_labels = "%b %y") +
  coord_cartesian(xlim = c(as.Date("2019-04-01"), as.Date("2019-08-01")),
                  ylim = c(0, round_maximum(zuko_data$Mass, 5))) +
  labs(x = NULL,
       y = "Mass (kg)") +
  NULL
```

Figure \@ref(fig:fig-mass-points) shows that the puppies have an exponential growth for the first 8 weeks. We will keep weighing Zuko, and see how this change. The graph should tapper off, however, I am not sure how long this will take. Before signing off, just a few pictures of our new puppy.

![](/post/2019-05-27-the-rainbows.en-gb_files/IMG_0966.JPG){width=50%}

![](/post/2019-05-30-the-rainbows.en-gb_files/IMG_0973.JPG){width=50%}

![](/post/2019-05-30-the-rainbows.en-gb_files/IMG_0974.JPG){width=50%}

![](/post/2019-05-30-the-rainbows.en-gb_files/IMG_0975.JPG){width=50%}

![](/post/2019-05-30-the-rainbows.en-gb_files/IMG_0976.JPG){width=50%}
