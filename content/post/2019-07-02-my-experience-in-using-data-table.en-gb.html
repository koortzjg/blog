---
title: My experience in using `data.table`
author: Hardi Koortzen
date: '2019-07-02'
slug: my-experience-in-using-data-table.en-gb
categories:
  - R
tags:
  - R
  - data.table
type: ''
subtitle: ''
image: ''
---



<p>My journey with <code>R</code> and <code>RStudio</code> started in 2014. At that stage, I was looking for a resource for working with Design of Experimental data. <code>R</code> was free, which was what I was looking for, since my company would not pay for a program for just evaluating one issue. I quickly grew to like the interface and versatility of <code>R</code>. It took me quite a bit of typing and learning, however, I persevered and could manage interesting analysis. My work took me via an interesting array of techniques and resources. I managed to create reproducible reports, and data analysis workflows. I also managed to get all the experimental data into an <code>SQL</code> database. Over the last couple of years, I have gotten used to the <code>tidyverse</code> way of doing things. This is a good way of learning the fundamentals, and for basic work, this is really good. I used it to create that previous mentioned database. There were times this was really slow, however, since I wanted to do it, I managed to find the time to accomplish this.</p>
<p>Since my new role, I started in March 2019, the data have been challenging. The data tends to be in the millions of rows, one of our platforms, contains about 20 million rows of data. Reading others’ blogs and twitter post, it seems that this would be a bit much for <code>dplyr</code> et al, thus a need for me to learn something new. This is where I started with <code>data.table</code>. This was a long winded introduction to my topic for this post, but it serve to introduce the reader to my <code>R</code> journey.</p>
<p>The main difference between the two styles, is all the verbs one get in the <code>tidyverse</code>. There is no <code>filter()</code> or <code>summarise()</code> functions for <code>data.table</code>. I was just starting to feel good about my abilities with <code>R</code>, and then I needed to start all over again. I enjoy learning new things, and sometimes the struggling is just about bearable, other times, it just feels like it is time to give in. In the end, I did persist, and now I am able to do some interesting summaries with <code>data.table</code>. The aim here is not to compare <code>tidyverse</code> with <code>data.table</code>, but rather an overview of my learnings in <code>data.table</code>. Hopefully, this will help me in the future, when I struggle again. I am not an advocate or vocal opposition of <code>base</code> <code>R</code>, <code>tidyverse</code> and/or <code>data.table</code>. I have used all, and know that they can all exists in the current environment. I also think that for more experience users, the <code>tidyverse</code> is not as user friendly, however, this is an excellent tool for beginners, and part time <code>R</code> user. Once you get hooked onto <code>R</code>, you will evolve and find your own way in the <code>base</code>, <code>tidyverse</code>, and <code>data.table</code> verse. I have recently come across this <a href="https://EdwinTh.github.io/blog/flamewars/">article</a>, regarding the psychology of flame wars.</p>
<p>The first few things that one needs to learn, is how to create new columns in a <code>data.table</code>. This can be accomplished by using the <code>:=</code> operator. This is useful, since we can create various columns, in the <code>data.table</code>, just by applying a grouping level. The grouping levels can be supplied in the <code>.()</code> function using <em>bare</em> names. There are times when the names needs to be quoted, however, I am now so used to this, thus I am using it all the time. OK, a quick example then, using the <code>airquality</code> dataset:</p>
<pre class="r"><code>suppressPackageStartupMessages(library(&quot;data.table&quot;))
# converting to a data.table object
airquality_dt &lt;- data.table(airquality)</code></pre>
<p>Looking at the dataset, it is clear that the temperate is in °F, so let’s change that to °C:</p>
<pre class="r"><code>airquality_dt[,
              TempC := (Temp - 32) / 1.8]</code></pre>
<p>That was easy right. We do not have to assign this back to <code>airquality_dt</code>, since this is done automatically with the <code>:=</code> function. It gets a bit confusing if you are looking at your data in the Environment window in <code>RStudio</code>, since after this operation, the number of variables does not change, however, the new column is there and is available for analysis. This is due to the fact that my setting is set to <code>Refresh Automatically</code>, and <code>data.table</code> does not write these back to the memory. After refreshing the Environment, this shows the new column, and all it’s properties. Now lets see if we could get the mean temperature for each month, and then for each day as well (note here not the actually date, just the column <code>Day</code>). Although this is a summarise-type calculation, we do not have to assign this to a new variable, we can just add a new column. This then calculates the aggregated functions on a group, in this case for <code>Month</code> and <code>Day</code>, respectively.</p>
<pre class="r"><code>airquality_dt[
  # calculate the mean temperature per &#39;Month&#39;
  ,
  MeanMonth := mean(TempC),
  .(Month)
  # piping into another caluclation
  ][
    # calculate the mean temperature per &#39;Day&#39;
    ,
    MeanDay := mean(TempC),
    .(Day)
    ]</code></pre>
<p>Here we can see that the mean was calculated for the month, <code>.(Month)</code>, and for the day, <code>.(Day)</code>. This also shows that chaining is possible. This might not be as readable as the <code>magrittr</code> pipes, but still very useful. The benefit here is that we now have the monthly and daily mean temperature in the table. Thus we can calculate some more variables based on these. If we only want to plot the means, we can create a new dataset, and only show the monthly data in it.</p>
<pre class="r"><code>monthly_airquality &lt;- airquality_dt[,
                                    .(MeanMonth = mean(TempC)),
                                    .(Month)]</code></pre>
<p>This dataset only contains 5 rows, and 2 columns. We achieve this summary with the <code>.()</code> function and without the <code>:=</code> function. This is similar to <code>dplyr</code>’s <code>summarise()</code> function. Summarising this way, one need to assign the output to a new variable or chain it into a plot function.</p>
<p>We can also select columns assigned to a variable. This might sometimes be usefully, especially, after reshaping the data to and from <em>long</em> to <em>wide</em>, or whatever, one wants to call that reshaping. In order for this to work, we need to include the <code>with = FALSE</code> in the <code>data.table</code> call.</p>
<pre class="r"><code>selected_columns &lt;-
  c(&quot;Temp&quot;, &quot;Month&quot;, &quot;Day&quot;, &quot;TempC&quot;, &quot;MeanMonth&quot;, &quot;MeanDay&quot;)
selected_airquality &lt;- airquality_dt[,
                                     selected_columns,
                                     with = FALSE]
names(selected_airquality)</code></pre>
<pre><code>## [1] &quot;Temp&quot;      &quot;Month&quot;     &quot;Day&quot;       &quot;TempC&quot;     &quot;MeanMonth&quot; &quot;MeanDay&quot;</code></pre>
<p>Joining data is useful, and is required in our daily work. Again, for <code>data.table</code> there is no verbs for this, however, there is an excellent tutorial on joining data with <code>data.table</code> <a href="https://rstudio-pubs-static.s3.amazonaws.com/52230_5ae0d25125b544caab32f75f0360e775.html">here</a>. I will not do it justice to just repeat what is already captured here.</p>
<p>There are times that we need to count the number of users, or rows, for a particular group. This is useful to calculate the number of users/instances of a variable. Next, we will be calculating the number of times each time value occurs in the dataset, using the <code>.N</code> function.</p>
<pre class="r"><code># converting to data.table object
chickweight &lt;- as.data.table(ChickWeight)
# calculating histogram data
chickweight_hist &lt;-
  chickweight[,
              .(count = .N),
              .(weight)
              ][
                # Left joining with a new data.table with a seq from 35 to 373
                data.table(weight = seq(35, 373, 1)),
                on = .(weight)
                ][
                  # Replacing the NA values with 0
                  ,
                  count := ifelse(is.na(count),
                                  0,
                                  count)]</code></pre>
<p>This code shows a combination of summarising, piping, creating a <code>data.table</code>, joining, and editing/creating a variable. First we count (<code>.N</code>) the number of rows by the <code>weight</code>, this gives us an histogram. Next we create a new <code>data.table</code>, containing a sequence of numbers from the start weight to the end weight (just to ensure a good plot, or to create a new binning sequence). This is then joined with the summarised data, and the rows with no values (<code>NA</code>’s) are replaced by zero, ensuring they are present in the plot. Now, to be honest here, I could just as well just plotted this with <code>geom_histogram()</code>, however, this was to show some of the capabilities of the <code>data.table</code> package.</p>
<div class="figure" style="text-align: centre"><span id="fig:hist-weight"></span>
<img src="/post/2019-07-02-my-experience-in-using-data-table.en-gb_files/figure-html/hist-weight-1.png" alt="Histogram of the chicken weights." width="95%" />
<p class="caption">
Figure 1: Histogram of the chicken weights.
</p>
</div>
<p>Other features that I have found useful, but did not discuss here, is the <code>uniqueN()</code> function and how to use the <code>.SD</code> function. The first is to count the unique number of a variable, while the latter can be used to calculate a subset of the <code>data.table</code>. I have probably not even touch on all the features, and maybe I will follow-up this post at some stage. For now, I think this is enough.</p>
